// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "database.h"

#include <QDebug>
#include <QTimer>
#include <QClipboard>
#include <QGuiApplication>
#include <QMimeData>

Database::Database(QObject *parent)
    : QAbstractListModel(parent)
    , m_impl(new DatabaseImpl())
{
}

void Database::open(const QString &password)
{
    beginResetModel();
    setDecrypting(true);
    m_impl->open(m_path, password);
    connect(m_impl, &DatabaseImpl::finished, this, [=](){
        setDecrypting(false);
        endResetModel();
    });
}

int Database::rowCount(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return m_impl->size();
}

QVariant Database::data(const QModelIndex &index, int role) const
{
    if(role == Title) {
        return m_impl->title(index.row());
    } else if(role == Username) {
        return m_impl->username(index.row());
    } else if(role == Password) {
        return m_impl->password(index.row());
    }
    return QVariant(0);
}

QHash<int, QByteArray> Database::roleNames() const
{
    QHash<int, QByteArray> roleNames;
    roleNames[Title] = "title";
    roleNames[Username] = "username";
    roleNames[Password] = "password";
    return roleNames;
}

void Database::copyPassword(const QString &password)
{
    QClipboard *clipboard = QGuiApplication::clipboard();
    auto mimeData = new QMimeData();
    mimeData->setData(QStringLiteral("x-kde-passwordManagerHint"), QByteArrayLiteral("secret"));
    mimeData->setData(QStringLiteral("text/plain"), password.toLocal8Bit());
    clipboard->setMimeData(mimeData);
    QTimer::singleShot(30000, this, [=](){
        clipboard->clear();
    });
}

KAboutData Database::aboutData() const
{
    return KAboutData::applicationData();
}

bool Database::decrypting() const
{
    return m_decrypting;
}

void Database::setDecrypting(bool decrypting)
{
    m_decrypting = decrypting;
    Q_EMIT decryptingChanged();
}

QString Database::path() const
{
    return m_path;
}

void Database::setPath(const QString &path)
{
    m_path = path;
    Q_EMIT pathChanged();
}
