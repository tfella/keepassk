<!--
  - SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
  - SPDX-License-Identifier: CC0-1.0
 -->
# Keepassk

A password manager for KDBX password databases.

Requires Qt > 5.12 and KI18n, Kirigami, KCoreAddons > 5.76

[Corrosion](https://github.com/AndrewGaspar/corrosion) is required for building the backend

Always build as release since debug builds are unusably slow.
